function sortedSquaredArray(array) {
    var newArr = new Array(array.length).fill(0);
      var firstIdx = 0;
      var lastIdx = array.length-1;
      
      for (let i = array.length-1; i >= 0 ; i--) {
          var first = Math.abs(array[firstIdx]);
          var last = Math.abs(array[lastIdx])
          if (first >= last) {
              newArr[i] = first**2;
              firstIdx++;
          } else {
              newArr[i] = last**2;
              lastIdx--;
          }
      }
    return newArr;
  }

  //ideal
//create an empty array and fill it with 0
//then loop through the array in reverse, and at each iteration, and maintain two pointers at the first and last indicies
//increment the indicies toward the center everytime you add one of the two to the new array (you add to the positional index of the reverse iteration, do not use unshift)
//as you increment and decrement your pointers, make sure to copmpare their absolute values and place them in reverse order