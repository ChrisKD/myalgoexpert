function isValidSubsequence(array, sequence) { //O(n) time, O(1) space
	var seqIdx = 0;
  for (let i = 0; i < array.length; i++) {
		if (array[i] === sequence[seqIdx]) {
			seqIdx++;
		}
		if (sequence[seqIdx] === seqIdx.length) return true;
	}
	return false;
}

//you loop through the longer array
//but you increment through the shorter array whenever you find that the value at the longer array's index is equal to the current value
//	of the shorter array
//by the end of the for loop, you should have full iterated through the full length of the shorter array, thus your count of increments should
//	equal its length
//this works because you are guaranteed to check in order on account of this double for loop
//also place a secondary condition for ending the for loop, in the event the shorter sequence is verified before the longer seequence is
//	complete